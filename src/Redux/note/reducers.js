const initState = {
    posts: [],
};
  
export default (state = initState, action) => {    
    switch (action.type) {
        case 'GET_ALL_POSTS':
            return {
                ...state,
                posts: action.posts
            };

        default:
            return state;
    }
};
