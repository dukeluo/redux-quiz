const getAllPosts = () => (dispatch) => {
    let URL = 'http://localhost:8080/api/posts';
    let info = {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }    
    };

    fetch(URL, info)
        .then(response => response.json())
        .then(result => {
            dispatch({
                type: 'GET_ALL_POSTS',
                posts: result,
            });
    }).catch(error => {
        console.error(error);
    });
};

const getAllPostsFromLocal = () => (dispatch) => {    
    dispatch({
        type: 'GET_ALL_POSTS_FROM_LOCAL',
    });
};

const deletePost = (id, callback) => (dispatch) => {
    let URL = `http://localhost:8080/api/posts/${id}`;
    let info = {
        method: 'DELETE',
        headers: {
            'Content-Type': 'application/json'
        }    
    };

    fetch(URL, info)
        .then(() => {
            dispatch({
                type: 'DELETE_SPECIFIED_POST',
            });
            callback();
    }).catch(error => {
        console.error(error);
    });
};

const createPost = (title, content, callback) => (dispatch) => {
    let URL = 'http://localhost:8080/api/posts';
    let data = {
        title,
        description: content,
    };
    let info = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(data),
    };

    fetch(URL, info)
        .then(() => {
            dispatch({
                type: 'CREATE_NEW_POST',
            });
            callback();
    }).catch(error => {
        console.error(error);
    });
};

export {
    getAllPosts,
    getAllPostsFromLocal,
    deletePost,
    createPost,
};