import {combineReducers} from "redux";
import noteReducer from './note/reducers';


const reducers = combineReducers({
    note: noteReducer,
});
export default reducers;