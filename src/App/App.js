import React, {Component} from 'react';
import './App.less';
import {BrowserRouter as Router} from "react-router-dom";
import {Route, Switch} from "react-router";
import Home from "../Components/Home/Home";
import NoteDetail from "../Components/NoteDetail/NoteDetail";
import NoteAddition from "../Components/NoteAddition/NoteAddition";

class App extends Component{
  render() {
    return (
      <div className='App'>
        <Router>
          <Switch>
            <Route path="/notes/create" component={NoteAddition} />
            <Route path="/notes/:id(\d+)" component={NoteDetail} />
            <Route path="/" component={Home}/>
          </Switch>
        </Router>
      </div>
    );
  }
}

export default App;