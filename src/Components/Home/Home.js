import React, { Component } from 'react';
import {connect} from "react-redux";
import {Link} from 'react-router-dom';
import {getAllPosts} from '../../Redux/note/actions';
import {bindActionCreators} from "redux";
import {MdNoteAdd} from 'react-icons/md';
import './Home.less';
import NoteHeader from '../NoteHeader/NoteHeader';

class Home extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        const noteArray = this.props.notes;
        const items = noteArray.map((item, index) => (
                <li className='list-item' key={index}>
                    <Link to={`/notes/${item.id}`}>
                        {item.title}
                    </Link>
                </li>
            )
        );

        return (
            <div className="Home">
                <NoteHeader />
                <ul>
                    {items}
                    <li className='add-note' key={noteArray.length}>
                        <Link to='/notes/create'>
                            <MdNoteAdd className='icon'/>
                        </Link>
                    </li>
                </ul>
            </div>
        );
    }

    componentDidMount() {
        this.props.getAllPosts();
    }
}

const mapStateToProps = state => ({
    notes: state.note.posts
});
  
const mapDispatchToProps = dispatch => bindActionCreators({
    getAllPosts
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Home);