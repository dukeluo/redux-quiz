import React, { Component } from 'react';
import {Link} from 'react-router-dom';
import {connect} from "react-redux";
import {getAllPostsFromLocal, deletePost} from '../../Redux/note/actions';
import returnHome from '../../Util/returnHome';
import {bindActionCreators} from "redux";
import NoteHeader from '../NoteHeader/NoteHeader';
import Button from '../Button/Button';
import './NoteDetail.less';

export class NoteDetails extends Component {
    constructor(props) {
        super(props);
        this.deleteNote = this.deleteNote.bind(this);
        this.toHome = this.toHome.bind(this);
    }

    deleteNote(id) {        
        this.props.deletePost(id, () => returnHome(this.props));
    }

    toHome() {
        returnHome(this.props);
    }

    render() {
        const id = +this.props.match.params.id;
        const noteArray = this.props.notes;        
        const titleItems = noteArray.map((item, index) => (
                <li className='list-item' key={index}>
                    <Link to={`/notes/${item.id}`}>
                        {item.title}
                    </Link>
                </li>
            )
        );
        const item = noteArray.find(item => item.id === id);

        return (
            <div className='NoteDetails'>
                <NoteHeader />
                <section className='titles'>
                    {titleItems}
                </section>
                <article className='note'>
                    <h2>{item.title}</h2>
                    <section>
                        {item.description}
                    </section>
                    <section className='button-set'>
                        <Button text='删除' handler={() => this.deleteNote(id)}/>
                        <Button text='返回' handler={this.toHome}/>
                    </section>
                </article>
            </div>
        );
    }

    componentDidMount() {
        this.props.getAllPostsFromLocal();
    }
}

const mapStateToProps = state => ({
    notes: state.note.posts
});

const mapDispatchToProps = dispatch => bindActionCreators({
    getAllPostsFromLocal,
    deletePost: (id, callback) => deletePost(id, callback),
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(NoteDetails);