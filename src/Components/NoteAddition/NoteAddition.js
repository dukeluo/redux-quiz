import React, { Component } from 'react';
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import NoteHeader from '../NoteHeader/NoteHeader';
import Button from '../Button/Button';
import returnHome from '../../Util/returnHome';
import { createPost } from '../../Redux/note/actions';
import './NoteAddition.less';

export class NoteAddition extends Component {
    constructor(props) {
        super(props);
        this.state = {
            title: '',
            content: '',
        };
        this.toHome = this.toHome.bind(this);
        this.inputChangeHandler = this.inputChangeHandler.bind(this);
        this.textAreaChangeHandler = this.textAreaChangeHandler.bind(this);
        this.addNote = this.addNote.bind(this);
    }

    toHome() {
        returnHome(this.props);
    }

    inputChangeHandler(event) {
        this.setState({
            title: event.target.value,
        });
    }

    textAreaChangeHandler(event) {
        this.setState({
            content: event.target.value,
        });
    }

    addNote() {
        this.props.createPost(this.state.title, this.state.content, () => returnHome(this.props));
    }

    render() {
        let disableSubmit = false;

        if (this.state.title.trim().length === 0 || this.state.content.trim().length === 0) {
            disableSubmit = true;
        }
        return (
            <div>
                <NoteHeader />
                <section className='note-addition'>
                    <h1>创建笔记</h1>
                    <section className='title'>
                        <h2>标题</h2>
                        <input type="text" onChange={this.inputChangeHandler} />
                    </section>
                    <section className="content">
                        <h2>正文</h2>
                        <textarea name="note-content" id="note-content" cols="30" rows="10" onChange={this.textAreaChangeHandler}></textarea>
                    </section>
                    <section className='button-set'>
                        <Button text='提交' handler={this.addNote} disabled={disableSubmit} />
                        <Button text='取消' handler={this.toHome} />
                    </section>
                </section>
            </div>
        );
    }
}

const mapDispatchToProps = dispatch => bindActionCreators({
    createPost: (title, content, callback) => createPost(title, content, callback),
}, dispatch);

export default connect(null, mapDispatchToProps)(NoteAddition);
