import React from 'react';
import './Button.less';

const Button = (props) => {
  return (
    <div className='button'>
      <button type='button' onClick={props.handler} disabled={props.disabled}>{props.text}</button>
    </div>
  );
}

export default Button;