import React from 'react';
import {MdViewList} from 'react-icons/md';
import './NoteHeader.less';

const NoteHeader = () => {
    return (
        <header className='note-header'>
            <MdViewList className='icon'/>
            <h3>notes</h3>
        </header>
    );
}

export default NoteHeader;